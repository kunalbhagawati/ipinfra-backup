# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.

from django.db import models


class AdminMappingGroupNode(models.Model):
    groupid = models.ForeignKey('AdminMasterGroup', db_column='groupid')
    nodeid = models.ForeignKey('AdminMasterNode', db_column='nodeid')
    createdon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin__mapping_group_node'


class AdminMappingModuleNode(models.Model):
    nodeid = models.ForeignKey('AdminMasterNode', db_column='nodeid')
    moduleid = models.ForeignKey('AdminMasterModule', db_column='moduleid')
    createdon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'admin__mapping_module_node'


class AdminMappingUserBranchAllocate(models.Model):
    userid = models.IntegerField()
    stateid = models.IntegerField()
    createdon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin__mapping_user_branch_allocate'


class AdminMappingUserGroup(models.Model):
    userid = models.ForeignKey('AdminMasterUser', db_column='userid')
    groupid = models.ForeignKey('AdminMasterGroup', db_column='groupid')
    createdon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin__mapping_user_group'


class AdminMappingUserNode(models.Model):
    userid = models.ForeignKey('AdminMasterUser', db_column='userid')
    nodeid = models.ForeignKey('AdminMasterNode', db_column='nodeid')
    createdon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin__mapping_user_node'


class AdminMasterGroup(models.Model):
    groupid = models.IntegerField(primary_key=True)
    groupname = models.CharField(max_length=255, blank=True)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin__master_group'


class AdminMasterModule(models.Model):
    moduleid = models.IntegerField(primary_key=True)
    modulename = models.CharField(max_length=255)
    createdon = models.DateTimeField()
    modifiedon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'admin__master_module'


class AdminMasterNode(models.Model):
    nodeid = models.IntegerField(primary_key=True)
    nodename = models.CharField(max_length=255, blank=True)
    parentid = models.IntegerField()
    createdon = models.DateTimeField()
    modifiedon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'admin__master_node'


class AdminMasterUser(models.Model):
    userid = models.IntegerField(primary_key=True)
    email = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    employeeid = models.IntegerField()
    branchassigned = models.IntegerField()
    reportingto = models.IntegerField()
    createdby = models.IntegerField()
    modifiedby = models.IntegerField()
    status = models.IntegerField()
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)
    lastlogin = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin__master_user'


class ListingCompleteness(models.Model):
    listingid = models.BigIntegerField()
    pricelevel = models.IntegerField()
    medialevel = models.IntegerField()
    listcontactinfolevel = models.IntegerField()
    paymentinfolevel = models.IntegerField()
    skulevel = models.IntegerField()
    winglevel = models.IntegerField()
    towerlevel = models.IntegerField()
    phaselevel = models.IntegerField()
    offerlevel = models.IntegerField()
    constructionlevel = models.IntegerField()
    banklevel = models.IntegerField()
    detailslevel = models.IntegerField()
    specificationlevel = models.IntegerField()
    amenitieslevel = models.IntegerField()
    projectlevel = models.IntegerField()
    locationlevel = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'listing_completeness'


class LogMappingTaskReason(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    taskid = models.IntegerField()
    reasonid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'log__mapping_task_reason'


class LogMappingTaskValue(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    taskid = models.IntegerField()
    valueid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'log__mapping_task_value'


class LogMasterActivity(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    userid = models.IntegerField()
    usertype = models.IntegerField()
    taskid = models.IntegerField()
    taskvaluemappingid = models.IntegerField(blank=True, null=True)
    taskreasonmappingid = models.IntegerField(blank=True, null=True)
    triggeredtime = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'log__master_activity'


class LogMasterModule(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    modulename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'log__master_module'


class LogMasterTaskreasons(models.Model):
    reasonid = models.IntegerField(primary_key=True)
    reasondescription = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'log__master_taskreasons'


class LogMasterTasks(models.Model):
    taskid = models.IntegerField(primary_key=True)
    taskdescription = models.CharField(max_length=45)
    parenttask = models.IntegerField(blank=True, null=True)
    hasvalue = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log__master_tasks'


class LogMasterTaskvalues(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    intvalue = models.IntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=45, blank=True)
    datatype = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'log__master_taskvalues'


class MappingAlert(models.Model):
    alertid = models.BigIntegerField(blank=True, null=True)
    alertfield = models.BigIntegerField(blank=True, null=True)
    alertintvalue = models.BigIntegerField(blank=True, null=True)
    alertstringvalue = models.CharField(max_length=255, blank=True)
    alertdatevalue = models.DateTimeField(blank=True, null=True)
    alerttextvalue = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'mapping_alert'


class MappingArea(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.ForeignKey('MasterProjectLevel', db_column='projectlevelid', blank=True, null=True)
    areaunitid = models.ForeignKey('MasterAreaunit', db_column='areaunitid', blank=True, null=True)
    areavalue = models.BigIntegerField(blank=True, null=True)
    areatypeid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'mapping_area'


class MappingBankProject(models.Model):
    bankid = models.BigIntegerField(blank=True, null=True)
    projectid = models.ForeignKey('Project', db_column='projectid', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_bank_project'


class MappingCitySeller(models.Model):
    cityid = models.IntegerField()
    sellerid = models.ForeignKey('MasterSeller', db_column='sellerid')

    class Meta:
        managed = False
        db_table = 'mapping_city_seller'


class MappingDealsInPropertyType(models.Model):
    sellerid = models.ForeignKey('MasterSeller', db_column='sellerid')
    propertytypeid = models.IntegerField()
    dealid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_deals_in_property_type'


class MappingLevelConstructionstatus(models.Model):
    id = models.BigIntegerField(primary_key=True)
    mappingid = models.IntegerField()
    projectlevelid = models.BigIntegerField()
    constructionstatusid = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_level_constructionstatus'


class MappingLevelSpecification(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    specificationid = models.IntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'mapping_level_specification'


class MappingLocalityGroup(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    groupid = models.IntegerField()
    localityid = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_locality_group'


class MappingLocationListing(models.Model):
    id = models.BigIntegerField(primary_key=True)
    locationcolumnid = models.IntegerField()
    listingid = models.BigIntegerField()
    value = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'mapping_location_listing'


class MappingMedia(models.Model):
    mediatypeid = models.BigIntegerField(blank=True, null=True)
    mappingid = models.BigIntegerField(blank=True, null=True)
    medianame = models.CharField(max_length=255, blank=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    mediadescription = models.TextField(blank=True)
    mediaurl = models.TextField(blank=True)
    displaypriorityorder = models.IntegerField(blank=True, null=True)
    validationstatus = models.CharField(max_length=1, blank=True)

    class Meta:
        managed = False
        db_table = 'mapping_media'


class MappingOtherlocalityListing(models.Model):
    localityname = models.CharField(max_length=50)
    listingid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'mapping_otherlocality_listing'


class MappingPgListingAmenities(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    amenityid = models.IntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'mapping_pg_listing_amenities'


class MappingPgListingDetails(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    detailid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_pg_listing_details'


class MappingPgListingSpecification(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    specificationid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_pg_listing_specification'


class MappingPrice(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    currencyid = models.BigIntegerField(blank=True, null=True)
    pricevalue = models.BigIntegerField(blank=True, null=True)
    ispricedisclosed = models.CharField(max_length=1)
    createdon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'mapping_price'


class MappingPriceArchive(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    currencyid = models.BigIntegerField(blank=True, null=True)
    pricevalue = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_price_archive'


class MappingRefreshdatesListing(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    listingid = models.IntegerField()
    tobeexpireddate = models.DateTimeField()
    createdon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'mapping_refreshdates_listing'


class MappingRentListingAmenities(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    amenityid = models.IntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_rent_listing_amenities'


class MappingRentListingDetails(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    detailid = models.IntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_rent_listing_details'


class MappingRentListingSpecification(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    specificationid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_rent_listing_specification'


class MappingSaleListingAmenities(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    amenityid = models.IntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_listing_amenities'


class MappingSaleListingDetails(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    detailid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_listing_details'


class MappingSaleListingSpecification(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    specificationid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_listing_specification'


class MappingSaleShowformAmenities(models.Model):
    amenityid = models.IntegerField(blank=True, null=True)
    propertylevelid = models.IntegerField(blank=True, null=True)
    propertytypeid = models.IntegerField(blank=True, null=True)
    sellertypeid = models.IntegerField()
    isrequired = models.CharField(max_length=1, blank=True)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_showform_amenities'


class MappingSaleShowformDetails(models.Model):
    detailid = models.IntegerField(blank=True, null=True)
    propertylevelid = models.IntegerField(blank=True, null=True)
    propertytypeid = models.IntegerField(blank=True, null=True)
    sellertypeid = models.IntegerField()
    isrequired = models.CharField(max_length=1, blank=True)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_showform_details'


class MappingSaleShowformSpecification(models.Model):
    specificationid = models.IntegerField(blank=True, null=True)
    propertylevelid = models.IntegerField(blank=True, null=True)
    propertytypeid = models.IntegerField(blank=True, null=True)
    sellertypeid = models.IntegerField()
    isrequired = models.CharField(max_length=1, blank=True)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_showform_specification'


class MappingSellerLocality(models.Model):
    id = models.BigIntegerField(primary_key=True)
    localityid = models.IntegerField()
    sellerid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'mapping_seller_locality'


class MappingSpecificationPropertySellerType(models.Model):
    specificationid = models.IntegerField(blank=True, null=True)
    propertytypeid = models.IntegerField(blank=True, null=True)
    sellertypeid = models.IntegerField()
    isrequired = models.CharField(max_length=1, blank=True)
    createdon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_specification_property_seller_type'


class MappingUserMedia(models.Model):
    mediatypeid = models.BigIntegerField(blank=True, null=True)
    mappingid = models.BigIntegerField(blank=True, null=True)
    medianame = models.CharField(max_length=255, blank=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    mediadescription = models.TextField(blank=True)
    mediaurl = models.TextField(blank=True)
    displaypriorityorder = models.IntegerField(blank=True, null=True)
    validationstatus = models.CharField(max_length=1, blank=True)

    class Meta:
        managed = False
        db_table = 'mapping_user_media'


class MappingUserMobile(models.Model):
    mappingid = models.BigIntegerField()
    userlevelid = models.IntegerField()
    countryphonecode = models.CharField(max_length=255, blank=True)
    areacode = models.CharField(max_length=6, blank=True)
    number = models.CharField(max_length=255, blank=True)
    ismobile = models.IntegerField()
    isverified = models.IntegerField()
    isprimary = models.CharField(max_length=1)
    temp_error_number = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'mapping_user_mobile'


class MappingUserSocialsite(models.Model):
    userid = models.BigIntegerField(blank=True, null=True)
    socialid = models.BigIntegerField(blank=True, null=True)
    secretekey = models.CharField(max_length=255, blank=True)
    token = models.CharField(max_length=255, blank=True)
    socialtype = models.CharField(max_length=255, blank=True)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_user_socialsite'


class MappingVirtualRealNumber(models.Model):
    id = models.BigIntegerField(primary_key=True)
    virtualnumber = models.BigIntegerField()
    realnumber = models.BigIntegerField()
    priority = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'mapping_virtual_real_number'


class MasterAlert(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    creationdate = models.DateTimeField(blank=True, null=True)
    modificationdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'master_alert'


class MasterAmenities(models.Model):
    amenityid = models.IntegerField(primary_key=True)
    parentid = models.IntegerField(blank=True, null=True)
    amenityname = models.CharField(max_length=255)
    isdependent = models.IntegerField(blank=True, null=True)
    createdon = models.DateTimeField()
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'master_amenities'


class MasterAreaType(models.Model):
    areatypeid = models.IntegerField(primary_key=True)
    areaname = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'master_area_type'


class MasterAreaunit(models.Model):
    areaunitid = models.BigIntegerField(primary_key=True)
    areaunitname = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_areaunit'


class MasterBank(models.Model):
    bankid = models.BigIntegerField(primary_key=True)
    bankname = models.CharField(max_length=255, blank=True)
    bankdescription = models.TextField(blank=True)
    banklogourl = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'master_bank'


class MasterCity(models.Model):
    cityid = models.IntegerField(primary_key=True)
    cityname = models.CharField(max_length=255)
    stateid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'master_city'


class MasterConstructionStatus(models.Model):
    constructionstatusid = models.BigIntegerField(primary_key=True)
    constructionstatusdescription = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_construction_status'


class MasterCountry(models.Model):
    countryid = models.IntegerField(db_column='countryId', primary_key=True)  # Field name made lowercase.
    countryname = models.CharField(db_column='countryName', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'master_country'


class MasterCurrency(models.Model):
    currencyid = models.BigIntegerField(primary_key=True)
    currencyname = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_currency'


class MasterDataType(models.Model):
    datatypeid = models.BigIntegerField(primary_key=True)
    datatypename = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_data_type'


class MasterDetails(models.Model):
    detailid = models.IntegerField(primary_key=True)
    parentid = models.IntegerField(blank=True, null=True)
    detailname = models.CharField(max_length=255)
    isdependent = models.IntegerField(blank=True, null=True)
    createdon = models.DateTimeField()
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'master_details'


class MasterListing(models.Model):
    listingid = models.BigIntegerField(primary_key=True)
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    sellerid = models.BigIntegerField(blank=True, null=True)
    actualsellerid = models.BigIntegerField()
    isipverified = models.IntegerField(blank=True, null=True)
    iscreatedbyadmin = models.IntegerField(blank=True, null=True)
    createdon = models.DateTimeField()
    expirydate = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'master_listing'


class MasterListingType(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    listingtype = models.CharField(max_length=20)
    parentid = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'master_listing_type'


class MasterLocality(models.Model):
    localityid = models.IntegerField(primary_key=True)
    localityname = models.CharField(max_length=255)
    cityid = models.IntegerField()
    stateid = models.IntegerField()
    countryid = models.IntegerField(blank=True, null=True)
    ispartofgroup = models.CharField(max_length=1, blank=True)

    class Meta:
        managed = False
        db_table = 'master_locality'


class MasterLocalityGroup(models.Model):
    groupid = models.IntegerField(primary_key=True)
    groupname = models.CharField(max_length=255)
    grouptype = models.CharField(max_length=7)
    oldid = models.IntegerField(db_column='oldId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'master_locality_group'


class MasterLocationColumn(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    columnname = models.CharField(max_length=255)
    propertytypeid = models.IntegerField()
    createdon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'master_location_column'


class MasterMediaType(models.Model):
    mediatypeid = models.BigIntegerField(primary_key=True)
    mediatypedescription = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_media_type'


class MasterMetaAlert(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    alertfield = models.CharField(max_length=50)
    alertfielddatatype = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'master_meta_alert'


class MasterProjectLevel(models.Model):
    projectlevelid = models.BigIntegerField(primary_key=True)
    projectlevelname = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_project_level'


class MasterPropertyType(models.Model):
    propertytypeid = models.IntegerField(primary_key=True)
    propertytypename = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_property_type'


class MasterRequirement(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    userid = models.IntegerField()
    listingtype = models.IntegerField()
    propertycategory = models.IntegerField()
    propertytype = models.CharField(max_length=200)
    state = models.IntegerField()
    city = models.IntegerField()
    locality = models.TextField()
    minprice = models.BigIntegerField()
    maxprice = models.BigIntegerField()
    monthlyrent = models.IntegerField()
    minbuiltuparea = models.BigIntegerField()
    maxbuiltuparea = models.BigIntegerField()
    builtupareameasurementunit = models.IntegerField()
    minplotarea = models.BigIntegerField()
    maxplotarea = models.BigIntegerField()
    plotareameasurementunit = models.IntegerField()
    noofbedrooms = models.CharField(max_length=25, blank=True)
    noofrestrooms = models.IntegerField()
    propertyage = models.IntegerField()
    carparking = models.IntegerField()
    willbuywithin = models.IntegerField()
    requestorgender = models.IntegerField()
    occupantscnt = models.IntegerField()
    occupantsgender = models.IntegerField()
    durationpgrentinmonths = models.IntegerField()
    furnished = models.IntegerField()
    attachedrestroom = models.IntegerField()
    aircondition = models.IntegerField()
    floor = models.IntegerField()
    roadfacing = models.IntegerField()
    comments = models.TextField()
    dateposted = models.DateTimeField()
    validated = models.IntegerField()
    authorized = models.IntegerField()
    status = models.IntegerField()
    referredby = models.BigIntegerField()
    deleted = models.IntegerField()
    datedeleted = models.DateTimeField()
    dateupdated = models.DateTimeField()
    smartsavers = models.IntegerField()
    preferedbuilder = models.CharField(max_length=150)
    preferedproject = models.CharField(max_length=150)
    transactiontype = models.IntegerField()
    paymentterms = models.IntegerField()
    campaignflag = models.IntegerField()
    alerttype = models.IntegerField()
    mailfrequency = models.IntegerField()
    timeframe = models.IntegerField()
    buyingparticulars = models.IntegerField()
    loanrequired = models.IntegerField()
    bouncecount = models.IntegerField()
    usersessionid = models.CharField(max_length=150)
    tabletype = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'master_requirement'


class MasterSeller(models.Model):
    sellerid = models.BigIntegerField(primary_key=True)
    userid = models.BigIntegerField()
    sellertypeid = models.IntegerField()
    name = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    companyname = models.CharField(max_length=255, blank=True)
    occupation = models.CharField(max_length=255, blank=True)
    stateid = models.BigIntegerField()
    cityid = models.BigIntegerField()
    websiteurl = models.CharField(max_length=255, blank=True)
    yearsofoccupation = models.IntegerField(blank=True, null=True)
    memberprofiledescription = models.TextField(blank=True)
    officeaddress = models.TextField(blank=True)
    facebookurl = models.CharField(max_length=255, blank=True)
    youtubeurl = models.CharField(max_length=255, blank=True)
    countryid = models.BigIntegerField()
    tollfreenumber = models.CharField(max_length=255, blank=True)
    isemailinvalid = models.IntegerField(blank=True, null=True)
    isphotoavailable = models.IntegerField(blank=True, null=True)
    islogoavailable = models.IntegerField(blank=True, null=True)
    isvideoavailable = models.IntegerField(blank=True, null=True)
    isvalidated = models.IntegerField(blank=True, null=True)
    validateon = models.DateTimeField(blank=True, null=True)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'master_seller'


class MasterSellerListingContactInfo(models.Model):
    listingcontactinfo = models.BigIntegerField(primary_key=True)
    listingid = models.BigIntegerField()
    contactpersonname = models.CharField(max_length=255)
    countryphonecode = models.CharField(max_length=50, blank=True)
    countryselected = models.CharField(max_length=50, blank=True)
    areacode = models.CharField(max_length=50, blank=True)
    phoneno = models.CharField(max_length=50, blank=True)
    preferredtime = models.TimeField(blank=True, null=True)
    email = models.CharField(max_length=255, blank=True)
    contactpreference = models.CharField(max_length=255, blank=True)
    enablec2cfor = models.CharField(max_length=255, blank=True)
    isemailvalid = models.IntegerField()
    tollfreenumber = models.CharField(max_length=255, blank=True)
    virtualnumber = models.CharField(max_length=255, blank=True)
    createdon = models.DateTimeField()
    modifiedon = models.DateTimeField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'master_seller_listing_contact_info'


class MasterSellerType(models.Model):
    sellertypeid = models.IntegerField()
    sellertypename = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'master_seller_type'


class MasterSpecification(models.Model):
    specificationid = models.IntegerField(primary_key=True)
    specificationname = models.CharField(max_length=255)
    parentid = models.IntegerField(blank=True, null=True)
    isdependent = models.IntegerField(blank=True, null=True)
    createdon = models.DateTimeField()
    modifiedon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'master_specification'


class MasterState(models.Model):
    stateid = models.IntegerField(primary_key=True)
    statename = models.CharField(max_length=255)
    countryid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'master_state'


class MasterSubseller(models.Model):
    subseller = models.BigIntegerField(primary_key=True)
    sellerid = models.BigIntegerField()
    userid = models.BigIntegerField(blank=True, null=True)
    mergeduserid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=255)
    accountname = models.CharField(max_length=255)
    email = models.CharField(max_length=255, blank=True)
    accountdescription = models.CharField(max_length=255, blank=True)
    userpaidstatus = models.IntegerField()
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField()
    validated = models.IntegerField(blank=True, null=True)
    validateon = models.DateTimeField(blank=True, null=True)
    lastlogin = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'master_subseller'


class MasterUser(models.Model):
    userid = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    password = models.CharField(max_length=255, blank=True)
    countryid = models.BigIntegerField(blank=True, null=True)
    stateid = models.BigIntegerField(blank=True, null=True)
    cityid = models.BigIntegerField(blank=True, null=True)
    occupation = models.CharField(max_length=255, blank=True)
    facebookid = models.CharField(max_length=255, blank=True)
    isemailvalid = models.IntegerField(blank=True, null=True)
    isphoneverified = models.IntegerField(blank=True, null=True)
    isvalidated = models.IntegerField(blank=True, null=True)
    isnri = models.IntegerField(blank=True, null=True)
    registrationipaddress = models.CharField(max_length=255, blank=True)
    validationdatetime = models.DateTimeField(blank=True, null=True)
    lastlogindatetime = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)
    createdon = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'master_user'


class MasterUserLevel(models.Model):
    userlevelid = models.IntegerField(primary_key=True)
    userlevelname = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'master_user_level'


class MasterUserType(models.Model):
    usertypeid = models.IntegerField(primary_key=True)
    usertypename = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'master_user_type'


class MasterVirtualNumber(models.Model):
    id = models.BigIntegerField(primary_key=True)
    virtualnumber = models.BigIntegerField()
    stateid = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'master_virtual_number'


class Offer(models.Model):
    offerid = models.BigIntegerField(primary_key=True)
    startdate = models.DateTimeField()
    enddate = models.DateTimeField()
    description = models.TextField(blank=True)
    imagepath = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'offer'


class Phase(models.Model):
    phaseid = models.BigIntegerField(primary_key=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    phasename = models.CharField(max_length=255, blank=True)
    phasedescription = models.TextField(blank=True)
    nooftowers = models.IntegerField(blank=True, null=True)
    landarea = models.TextField(blank=True)
    launchdate = models.DateTimeField(blank=True, null=True)
    deliverydate = models.DateTimeField(blank=True, null=True)
    isconstructionstatusavailable = models.CharField(max_length=1, blank=True)

    class Meta:
        managed = False
        db_table = 'phase'


class Project(models.Model):
    projectid = models.BigIntegerField(primary_key=True)
    projectname = models.CharField(max_length=255, blank=True)
    projectdescription = models.TextField(blank=True)
    skuarearange = models.TextField(blank=True)
    projectarea = models.TextField(blank=True)
    noofphase = models.IntegerField(blank=True, null=True)
    isvirtualnumberavailable = models.CharField(max_length=1, blank=True)
    isbankapproved = models.CharField(max_length=1, blank=True)
    isofferavailable = models.CharField(max_length=1, blank=True)
    isconstructionstatusavailable = models.CharField(max_length=1, blank=True)
    sellerid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'project'


class Sku(models.Model):
    skuid = models.BigIntegerField(primary_key=True)
    wingid = models.BigIntegerField(blank=True, null=True)
    towerid = models.BigIntegerField(blank=True, null=True)
    phaseid = models.BigIntegerField(blank=True, null=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    propertytypeid = models.IntegerField(blank=True, null=True)
    possession = models.DateTimeField(blank=True, null=True)
    propertyagedatetime = models.DateTimeField(blank=True, null=True)
    areashowcase = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'sku'


class Tower(models.Model):
    towerid = models.BigIntegerField(primary_key=True)
    phaseid = models.BigIntegerField(blank=True, null=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    towername = models.CharField(max_length=255, blank=True)
    towerdescription = models.TextField(blank=True)
    noofwings = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tower'


class Unit(models.Model):
    unitid = models.BigIntegerField(primary_key=True)
    skuid = models.BigIntegerField(blank=True, null=True)
    wingid = models.BigIntegerField(blank=True, null=True)
    towerid = models.BigIntegerField(blank=True, null=True)
    phaseid = models.BigIntegerField(blank=True, null=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    sellerid = models.BigIntegerField()
    displayprice = models.CharField(max_length=1, blank=True)

    class Meta:
        managed = False
        db_table = 'unit'


class Vidteq(models.Model):
    vidteqid = models.BigIntegerField(primary_key=True)
    src_lat = models.CharField(max_length=15)
    src_long = models.CharField(max_length=15)
    dest_lat = models.CharField(max_length=15)
    dest_long = models.CharField(max_length=15)
    description = models.TextField(blank=True)
    projectid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'vidteq'


class VirtualNumberBuilder(models.Model):
    id = models.BigIntegerField(primary_key=True)
    virtualnumber = models.BigIntegerField()
    sellerid = models.BigIntegerField()
    cityid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'virtual_number_builder'


class Wing(models.Model):
    wingid = models.BigIntegerField(primary_key=True)
    towerid = models.BigIntegerField(blank=True, null=True)
    phaseid = models.BigIntegerField(blank=True, null=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    wingname = models.CharField(max_length=255, blank=True)
    wingdescription = models.TextField(blank=True)
    nooftypeofsku = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wing'
