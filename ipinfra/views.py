from django.apps import apps
from django.http import HttpResponse
import json


def index(request):
    response = "Just a demo index page"
    return HttpResponse(response)
