from rest_framework import serializers
from . import models


class ListingTypeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.MasterListingType


class ListingSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.MasterListing
