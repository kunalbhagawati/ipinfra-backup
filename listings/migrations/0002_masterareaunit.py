# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasterAreaunit',
            fields=[
                ('areaunitid', models.BigIntegerField(primary_key=True, serialize=False)),
                ('areaunitname', models.CharField(max_length=255, blank=True)),
            ],
            options={
                'db_table': 'master_areaunit',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
