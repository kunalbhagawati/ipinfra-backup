from django.http import HttpResponse
import json


def index(request):
    availableRequests = ('listingTypes')
    response = json.dumps(availableRequests)
    return HttpResponse(response)
