from django.conf.urls import patterns, url
from . import views, index

urlpatterns = patterns('',
    # url(r'^$', index.index),
    url(r'^listingtypes/$', views.listingTypes),
    url(r'^$', views.listings),
    url(r'^(?P<pk>[0-9]+)$', views.listing_detail),
)
