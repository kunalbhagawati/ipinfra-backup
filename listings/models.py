# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.

from django.db import models


class MasterAreaunit(models.Model):
    areaunitid = models.BigIntegerField(primary_key=True)
    areaunitname = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_areaunit'


class ListingCompleteness(models.Model):
    listingid = models.BigIntegerField()
    pricelevel = models.IntegerField()
    medialevel = models.IntegerField()
    listcontactinfolevel = models.IntegerField()
    paymentinfolevel = models.IntegerField()
    skulevel = models.IntegerField()
    winglevel = models.IntegerField()
    towerlevel = models.IntegerField()
    phaselevel = models.IntegerField()
    offerlevel = models.IntegerField()
    constructionlevel = models.IntegerField()
    banklevel = models.IntegerField()
    detailslevel = models.IntegerField()
    specificationlevel = models.IntegerField()
    amenitieslevel = models.IntegerField()
    projectlevel = models.IntegerField()
    locationlevel = models.IntegerField()
    x = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'listing_completeness'


class MappingArea(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.ForeignKey('MasterProjectLevel', db_column='projectlevelid', blank=True, null=True)
    areaunitid = models.ForeignKey('MasterAreaUnit', db_column='areaunitid', blank=True, null=True)
    areavalue = models.BigIntegerField(blank=True, null=True)
    areatypeid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'mapping_area'


class MappingBankProject(models.Model):
    bankid = models.BigIntegerField(blank=True, null=True)
    projectid = models.ForeignKey('Project', db_column='projectid', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_bank_project'


class MappingLevelConstructionstatus(models.Model):
    id = models.BigIntegerField(primary_key=True)
    mappingid = models.IntegerField()
    projectlevelid = models.BigIntegerField()
    constructionstatusid = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_level_constructionstatus'


class MappingLevelSpecification(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    specificationid = models.IntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'mapping_level_specification'


class MappingLocationListing(models.Model):
    id = models.BigIntegerField(primary_key=True)
    locationcolumnid = models.IntegerField()
    listingid = models.BigIntegerField()
    value = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'mapping_location_listing'


class MappingOtherlocalityListing(models.Model):
    localityname = models.CharField(max_length=50)
    listingid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'mapping_otherlocality_listing'


class MappingPgListingAmenities(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    amenityid = models.IntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'mapping_pg_listing_amenities'


class MappingPgListingDetails(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    detailid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_pg_listing_details'


class MappingPgListingSpecification(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    specificationid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_pg_listing_specification'


class MappingPrice(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    currencyid = models.BigIntegerField(blank=True, null=True)
    pricevalue = models.BigIntegerField(blank=True, null=True)
    ispricedisclosed = models.CharField(max_length=1)
    createdon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'mapping_price'


class MappingPriceArchive(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    currencyid = models.BigIntegerField(blank=True, null=True)
    pricevalue = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_price_archive'


class MappingRefreshdatesListing(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    listingid = models.IntegerField()
    tobeexpireddate = models.DateTimeField()
    createdon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'mapping_refreshdates_listing'


class MappingRentListingAmenities(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    amenityid = models.IntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_rent_listing_amenities'


class MappingRentListingDetails(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    detailid = models.IntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_rent_listing_details'


class MappingRentListingSpecification(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    specificationid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_rent_listing_specification'


class MappingSaleListingAmenities(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    amenityid = models.IntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_listing_amenities'


class MappingSaleListingDetails(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    detailid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_listing_details'


class MappingSaleListingSpecification(models.Model):
    mappingid = models.BigIntegerField(blank=True, null=True)
    listingid = models.BigIntegerField(blank=True, null=True)
    specificationid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    stringvalue = models.CharField(max_length=255, blank=True)
    intvalue = models.BigIntegerField(blank=True, null=True)
    datevalue = models.DateTimeField(blank=True, null=True)
    entryvalidationstatus = models.CharField(max_length=1)
    createdon = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mapping_sale_listing_specification'


class MasterListing(models.Model):
    listingid = models.BigIntegerField(primary_key=True)
    mappingid = models.BigIntegerField(blank=True, null=True)
    projectlevelid = models.BigIntegerField(blank=True, null=True)
    sellerid = models.BigIntegerField(blank=True, null=True)
    actualsellerid = models.BigIntegerField()
    isipverified = models.IntegerField(blank=True, null=True)
    iscreatedbyadmin = models.IntegerField(blank=True, null=True)
    createdon = models.DateTimeField()
    expirydate = models.DateTimeField(blank=True, null=True)
    modifiedon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'master_listing'


class MasterListingType(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    listingtype = models.CharField(max_length=20)
    parentid = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'master_listing_type'


class MasterLocationColumn(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    columnname = models.CharField(max_length=255)
    propertytypeid = models.IntegerField()
    createdon = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'master_location_column'


class MasterProjectLevel(models.Model):
    projectlevelid = models.BigIntegerField(primary_key=True)
    projectlevelname = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'master_project_level'


class MasterSellerListingContactInfo(models.Model):
    listingcontactinfo = models.BigIntegerField(primary_key=True)
    listingid = models.BigIntegerField()
    contactpersonname = models.CharField(max_length=255)
    countryphonecode = models.CharField(max_length=50, blank=True)
    countryselected = models.CharField(max_length=50, blank=True)
    areacode = models.CharField(max_length=50, blank=True)
    phoneno = models.CharField(max_length=50, blank=True)
    preferredtime = models.TimeField(blank=True, null=True)
    email = models.CharField(max_length=255, blank=True)
    contactpreference = models.CharField(max_length=255, blank=True)
    enablec2cfor = models.CharField(max_length=255, blank=True)
    isemailvalid = models.IntegerField()
    tollfreenumber = models.CharField(max_length=255, blank=True)
    virtualnumber = models.CharField(max_length=255, blank=True)
    createdon = models.DateTimeField()
    modifiedon = models.DateTimeField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'master_seller_listing_contact_info'


class Phase(models.Model):
    phaseid = models.BigIntegerField(primary_key=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    phasename = models.CharField(max_length=255, blank=True)
    phasedescription = models.TextField(blank=True)
    nooftowers = models.IntegerField(blank=True, null=True)
    landarea = models.TextField(blank=True)
    launchdate = models.DateTimeField(blank=True, null=True)
    deliverydate = models.DateTimeField(blank=True, null=True)
    isconstructionstatusavailable = models.CharField(max_length=1, blank=True)

    class Meta:
        managed = False
        db_table = 'phase'


class Project(models.Model):
    projectid = models.BigIntegerField(primary_key=True)
    projectname = models.CharField(max_length=255, blank=True)
    projectdescription = models.TextField(blank=True)
    skuarearange = models.TextField(blank=True)
    projectarea = models.TextField(blank=True)
    noofphase = models.IntegerField(blank=True, null=True)
    isvirtualnumberavailable = models.CharField(max_length=1, blank=True)
    isbankapproved = models.CharField(max_length=1, blank=True)
    isofferavailable = models.CharField(max_length=1, blank=True)
    isconstructionstatusavailable = models.CharField(max_length=1, blank=True)
    sellerid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'project'


class Sku(models.Model):
    skuid = models.BigIntegerField(primary_key=True)
    wingid = models.BigIntegerField(blank=True, null=True)
    towerid = models.BigIntegerField(blank=True, null=True)
    phaseid = models.BigIntegerField(blank=True, null=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    propertytypeid = models.IntegerField(blank=True, null=True)
    possession = models.DateTimeField(blank=True, null=True)
    propertyagedatetime = models.DateTimeField(blank=True, null=True)
    areashowcase = models.CharField(max_length=255, blank=True)

    class Meta:
        managed = False
        db_table = 'sku'


class Tower(models.Model):
    towerid = models.BigIntegerField(primary_key=True)
    phaseid = models.BigIntegerField(blank=True, null=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    towername = models.CharField(max_length=255, blank=True)
    towerdescription = models.TextField(blank=True)
    noofwings = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tower'


class Unit(models.Model):
    unitid = models.BigIntegerField(primary_key=True)
    skuid = models.BigIntegerField(blank=True, null=True)
    wingid = models.BigIntegerField(blank=True, null=True)
    towerid = models.BigIntegerField(blank=True, null=True)
    phaseid = models.BigIntegerField(blank=True, null=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    sellerid = models.BigIntegerField()
    displayprice = models.CharField(max_length=1, blank=True)

    class Meta:
        managed = False
        db_table = 'unit'


class Wing(models.Model):
    wingid = models.BigIntegerField(primary_key=True)
    towerid = models.BigIntegerField(blank=True, null=True)
    phaseid = models.BigIntegerField(blank=True, null=True)
    projectid = models.BigIntegerField(blank=True, null=True)
    wingname = models.CharField(max_length=255, blank=True)
    wingdescription = models.TextField(blank=True)
    nooftypeofsku = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wing'
