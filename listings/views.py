# # django

# # REST Framework
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response

# # Listings App
import listings.models as models 
import listings.serializers as serializers


@api_view(['GET', ])
def listingTypes(request):
    if request.method == 'GET':
        listingsTypes = models.MasterListingType.objects.all()
        serializer = serializers.ListingTypeSerializer(listingsTypes, many=True)
        return Response(serializer.data)


@api_view(['GET', 'POST'])
def listings(request):
    if request.method == 'GET':
        listingsList = models.MasterListing.objects.all()[:100]
        serializer = serializers.ListingSerializer(listingsList, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = serializers.ListingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def listing_detail(request, pk):
    """
    Get details, update or delete a listing.
    """
    try:
        listing = models.MasterListing.objects.get(pk=pk)
    except models.MasterListing.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = serializers.ListingSerializer(listing)
        return Response(serializer.data)
