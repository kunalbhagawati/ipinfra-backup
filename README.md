# IP Infra layer APIs #

Holds the code for the IP infra layer. 
    
    
### Note ###

This repo holds only two APIs, and only their GET requests (Read Ops). This is only meant to be a guide if you are starting the REST framework for the first time. 
The URLs for these are:  
*  `http://localhost:8000/listings/`  
*  `http://localhost:8000/listings/<listingidhere>`  
   
     
If you want more working examples checkout the development branch 'ListingSerializers' at commit '73311a2'  
    `git checkout ListingSerializers && git checkout 73311a2`  

Note that this branch is unstable and you may run into bugs.

--------

## **Setup** ##

* ### PIP Dependancies ###
    -  python dependancies:
        +  build-essential (for the internal compilation of some modules)
        +  libmysqlclient-dev (mysql client driver/internal connector) 
        +  python-mysqldb (no idea, ask mukesh)

        These can be installed directly with  
        `apt-get install build-essential python-dev libmysqlclient-dev python-mysqldb`  
    
        
* ### **Requires** ###
    - Django==1.7.1
    - djangorestframework==3.0.2
    - httpie==0.8.0
    - ipython==2.3.1
    - requests==2.5.0
    - mysqlclient==1.3.4  
    
    
* ### **Database configuration** ###
Uses mysql database with the mysql connection below. Change to order..
    
```
#!python

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': '',
        'NAME': 'indiaproperty',
        'CONN_MAX_AGE': 0,
        'PASSWORD': 'root',
        'USER': 'root',
    }
}


```


* ### **Deployment instructions** ###
Still in Progress  
    
----------  

### My playlist :\) ###
* Unn Arugil Vaa - Kaloori [https://www.youtube.com/watch?v=iwXdO0VA3r0]
* Come and Get It - Krewella (Razihel Remix) [https://www.youtube.com/watch?v=2LeKMGwNaqs]
* Tu Hai Ki Nahi - Roy [https://www.youtube.com/watch?v=_Ufd60sTMK8]
* Mortal Share - Insomnium [https://www.youtube.com/watch?v=W1OXcH1bg20]
* Rabba - Coke Studio India [https://www.youtube.com/watch?v=xjQMFqiMjxk]
* Mazhaiye Mazhaiye - Eeram [https://www.youtube.com/watch?v=bwx4lsbthf8]
* A Greater Foundation - As I Lay Dying [https://www.youtube.com/watch?v=LQSOcuZR4Jc]